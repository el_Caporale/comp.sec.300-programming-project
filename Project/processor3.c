#include "stdlib.h"

#include "parser.h"
#include "processor3.h"


void *processor3_work(void *vargp)
{
	// Get the context
	proc_ctx_t* ctx = (proc_ctx_t*)vargp;

	OUTPUT("Worker 3: Final hash\n");

	for(uint32_t i=0; i<32; i++)
	{
		OUTPUT("%02x", ctx->hash_result[i]);
	}
	OUTPUT("\n");

	// Free the context and buffer
	free(ctx);

	return 0;
}