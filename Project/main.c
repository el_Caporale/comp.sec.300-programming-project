#include <string.h>
#include "stdbool.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#if defined(USE_UT)
#  include "unittests.h"
#endif

#include "parser.h"



#if !defined(USE_UT)

// libFuzzer interface
#if defined (USE_FUZZ_IF_LIBFUZZ)
int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) 
{
    // Start the parsing
    parseData((uint8_t*)data, size);
    return 0;
}
// AFL interface
#elif defined(USE_FUZZ_IF_AFL)
int main(int argc, char const *argv[])
{
    char data[SOCK_MAXDATA];
    if(argc>1)
    {
            FILE *fp = fopen(argv[1], "r");
            fgets(data, sizeof(data), fp);
    }
    else
    {
            fgets(data, sizeof(data), stdin);
    }
    // Start the parsing
    parseData(data, sizeof(data));
}
// main interface
#else
int main(int argc, char const *argv[])
{
    pthread_t thread_id;

    printf("Start parser Thread\n");
    pthread_create(&thread_id, NULL, parserThread, NULL);
    // Block until finished
    pthread_join(thread_id, NULL);
    return 0;
}
#endif

#else // USE_UT
int main(int argc, char const *argv[])
{
    ut_main(argc, argv);
    return 0;
}
#endif