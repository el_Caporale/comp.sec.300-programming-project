#include "stdlib.h"

#include "parser.h"
#include "processor1.h"
#include "processor2.h"
#include "string.h"


void processor1_work(char* pData, int dataLen)
{
	pthread_t thread_p2;

	OUTPUT("Worker 1 working\n");

	// Allocate buffer for the context and data
	uint8_t* proc_buf = malloc(sizeof(proc_ctx_t) + dataLen);

	if(!proc_buf)
	{
		return;
	}
	memcpy(proc_buf + sizeof(proc_ctx_t), pData, dataLen);
	proc_ctx_t* ctx = (proc_ctx_t*)proc_buf;

	// Fill the context
	ctx->proc_buf_len = dataLen + sizeof(proc_ctx_t);

	// Pass the context to the next thread
	pthread_create(&thread_p2, NULL, processor2_work, ctx);
	return;
}