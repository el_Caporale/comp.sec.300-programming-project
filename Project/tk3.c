#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "tk3.h"
#include "skinny.h"

//! Some shortcuts for uint
typedef uint8_t  U1;
typedef uint16_t U2;
typedef uint32_t U4;
typedef uint64_t U8;

// SKINNY-tk3-Hash block length
#define BLOCK_LEN 16


// XOR two chunks of data, update first one with results
void XOR_data_chunks(U1* inout, U1* in, U4 len)
{
    for(U4 i=0; i<len; i++)
    {
        inout[i] ^= in[i];
    }
}

// F384 function primitive
void F384(U1* in, U1* out)
{
    U1 pt1[BLOCK_LEN] = {0};
    U1 pt2[BLOCK_LEN] = {0};
    U1 pt3[BLOCK_LEN] = {0};
    
    // Prepare the inputs
    pt2[0] = 0x1;
    pt3[0] = 0x2;
    
    // Get the ciphertexts
    skinny(out, pt1, in);
    skinny(out + BLOCK_LEN,   pt2, in);
    skinny(out + BLOCK_LEN*2, pt3, in);
}


// Function that uses F384 primitive of the SKINNY hash, works with the hash state in-place
void proc_state(U1* inout)
{
    U1 buffer[48];

    // Process state
    F384(inout, buffer);
    
    // Copy result to the same place
    memcpy(inout, buffer, sizeof(buffer));
}

// Add padding to the full block
void addPad(U4 dataLen, U1* full_block)
{
    full_block[dataLen] = 0x80;
}

// Initialize the context
void init(context *ctx)
{
    memset(ctx->state, 0x00, sizeof(ctx->state));
    ctx->state[BLOCK_LEN] = 0x80;
    
    memset(ctx->bufferedData, 0x00, sizeof(ctx->bufferedData));
    ctx->bufferedDataLen = 0x00;
}

// Update the context with new data
void update(const unsigned char *a, int len, context *ctx)
{
    U1 workBlock[BLOCK_LEN];
    U4 dataLeft = ctx->bufferedDataLen + len;
    
    if(dataLeft >= BLOCK_LEN)
    {
        // Offset considering buffered data
        U4 offset = ctx->bufferedDataLen;
        U4 inputBlocks = dataLeft / BLOCK_LEN;
        
        // Work first block
        memcpy(workBlock, ctx->bufferedData, ctx->bufferedDataLen);
        memcpy(workBlock + ctx->bufferedDataLen, a, BLOCK_LEN - offset);
        
        // XOR and update
        XOR_data_chunks((U1*)ctx->state, workBlock, BLOCK_LEN);
        proc_state(ctx->state);
        dataLeft -= BLOCK_LEN;
        
        // Reset the buffer
        ctx->bufferedDataLen = 0;
        memset(ctx->bufferedData, 0x00, BLOCK_LEN);
        
        // Absorb the input data full blocks, if any
        for(U4 i=1; i<inputBlocks; i++)
        {
            // Copy to work block
            memcpy(workBlock, (U1*)(a+i*16), BLOCK_LEN);

            // XOR and update
            XOR_data_chunks((U1*)ctx->state, workBlock, BLOCK_LEN);
            proc_state(ctx->state);
            dataLeft -= BLOCK_LEN;
        }
    }
    else
    {
        memcpy(ctx->bufferedData + ctx->bufferedDataLen, (U1*)a, dataLeft - ctx->bufferedDataLen);
        ctx->bufferedDataLen += dataLeft - ctx->bufferedDataLen;
        return;
    }
    
    
    // Buffer leftover data
    if(dataLeft)
    {
        ctx->bufferedDataLen += dataLeft;
        memcpy(ctx->bufferedData, a + len - dataLeft, dataLeft);
    }
}

// finalize the context and output data
void finalize(unsigned char *a, context *ctx)
{
    // Pad the last block
    addPad(ctx->bufferedDataLen, ctx->bufferedData);
    
    // XOR and update
    XOR_data_chunks((U1*)ctx->state, ctx->bufferedData, BLOCK_LEN);
    proc_state((U1*)ctx->state);
    
    // Copy first half
    memcpy(a, ctx->state, BLOCK_LEN);

    proc_state((U1*)ctx->state);

    // Copy the second half
    memcpy((U1*)(a + BLOCK_LEN),  (U1*)ctx->state, BLOCK_LEN);
}
