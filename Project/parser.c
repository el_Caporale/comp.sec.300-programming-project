#include <unistd.h>
#include <stdio.h>
#ifdef __WIN32__
#  include <winsock2.h>
#  include <ws2tcpip.h>
#else
# include <sys/socket.h>
#endif
#include <stdlib.h>
#ifndef __WIN32__
#  include <netinet/in.h>
#endif
#include <string.h>
#include "stdbool.h"

#include "parser.h"

#include "processor1.h"
#include "processor2.h"
#include "processor3.h"

#define SOCK_HELLO_MESSAGE   "Hello from server"


int socket_setup(int* pServer_fd, int port)
{
    int opt = 1;

    // Create socket file descriptor
    if ((*pServer_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        ERROR("socket failed");
        exit(EXIT_FAILURE);
    }

    // Attach socket to the port
    if (setsockopt(*pServer_fd, SOL_SOCKET, SO_REUSEADDR,
                   &opt, sizeof(opt)))
    {
        ERROR("setsockopt");
        exit(EXIT_FAILURE);
    }
    return 0;
}


int socket_bind_and_accept(struct sockaddr_in address, int* pSocket, int server_fd, int port)
{
    int addrlen = sizeof(address);

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( port );

    // Attach socket to the port
    if (bind(server_fd, (struct sockaddr *)&address,
                                 sizeof(address))<0)
    {
        ERROR("bind failed");
        exit(EXIT_FAILURE);
    }
    if (listen(server_fd, 3) < 0)
    {
        ERROR("listen");
        exit(EXIT_FAILURE);
    }
    if ((*pSocket = accept(server_fd, (struct sockaddr *)&address,
                       (socklen_t*)&addrlen))<0)
    {
        ERROR("accept");
        exit(EXIT_FAILURE);
    }
    return 0;
}


int parseData(char* pData, int dataLen)
{
    if(   dataLen
       && *pData == (char)'s'
    )
    {
        processor1_work(pData, dataLen);
    }
    return 0;
}

void *parserThread(void *vargp)
{
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    
    char buffer[SOCK_MAXDATA] = {0};
    char *hello = SOCK_HELLO_MESSAGE;


    if(socket_setup(&server_fd, SOCK_PORT))
    {
        ERROR("socket_setup failed\n");
        return NULL;
    }

    if(socket_bind_and_accept(address, &new_socket, server_fd, SOCK_PORT))
    {
        ERROR("socket_bind_and_accept failed\n");
        return NULL;
    }


    while(true)
    {
        valread = read( new_socket , buffer, SOCK_MAXDATA);
        printf("%s\n",buffer );
        parseData(buffer, valread);
        send(new_socket , hello , strlen(hello) , 0 );
    }
}