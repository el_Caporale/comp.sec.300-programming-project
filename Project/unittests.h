#ifndef UNITTESTS_H
#define UNITTESTS_H

#if defined(USE_UT)
void ut_main(int argc, char const *argv[]);
#endif

#endif // UNITTESTS_H