#include "stdlib.h"

#include "parser.h"
#include "processor2.h"
#include "processor3.h"

#include "tk3.h"


void *processor2_work(void *vargp)
{
	pthread_t thread_p3;

	// Get the context
	proc_ctx_t* ctx = (proc_ctx_t*)vargp;

	OUTPUT("Worker 2 working\n");

	context skinny_ctx;
	init(&skinny_ctx);

	// Hash data
	for(uint32_t i=0; i<(ctx->proc_buf_len-sizeof(proc_ctx_t)); i++)
	{
		update((const unsigned char *)(ctx + sizeof(proc_ctx_t) + i), 1, &skinny_ctx);
	}

	// Final
	finalize(ctx->hash_result, &skinny_ctx);

	// Pass the context to the next thread
	pthread_create(&thread_p3, NULL, processor3_work, ctx);
    // Block until finished
    pthread_join(thread_p3, NULL);

	return 0;
}