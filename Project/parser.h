#ifndef PARSER_H
#define PARSER_H

#include <pthread.h>
#include "stdio.h"
#include "stdint.h"

#define SOCK_MAXDATA         1024
#define SOCK_PORT            8080

// Silence the non-critical printfs in fuzzers to prevent spam
#if defined(USE_FUZZ_IF_LIBFUZZ) || defined(USE_FUZZ_IF_AFL)
#  define OUTPUT(...)
#else
#  define OUTPUT(...) printf(__VA_ARGS__)
#endif

// Abort fuzzer runs on error, print on regular run
#if defined(USE_FUZZ_IF_AFL)
#  define ERROR(...) \
	printf(__VA_ARGS__); \
	abort();
#elif defined(USE_FUZZ_IF_LIBFUZZ)
#  define ERROR(...) \
	printf(__VA_ARGS__); \
	__builtin_trap();
#else
#  define ERROR(...) printf(__VA_ARGS__);
#endif


typedef struct {
	uint32_t 	proc_buf_len;
	uint8_t 	hash_result[32];
//	uint8_t 	proc_buf[proc_buf_len];
} proc_ctx_t;


int parseData(char* pData, int dataLen);

void *parserThread(void *vargp);

#endif // PARSER_H