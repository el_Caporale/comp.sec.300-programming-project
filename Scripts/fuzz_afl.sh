mkdir afl
cd afl
mkdir afl_in
cd afl_in
openssl rand -hex 1024 > in.txt
cd .. && mkdir afl_out 
afl-gcc -pthread ../Project/*.[ch] -DUSE_FUZZ_IF_AFL
timeout -s INT 1m afl-fuzz -i afl_in -o afl_out -- ./a.out